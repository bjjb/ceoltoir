Websites for musicians and bands
--------------------------------

Ceoltoir will be a Ruby on Rails application for hosting musicians' or bands'
websites - a sort of personal [MySpace](http://myspace.com) that doesn't suck,
for one artist - where they can upload their songs, announce tour dates, send
out newsletters, and receive comments from their fans.

The sites running on Ceoltoir will be completely themable, but all expose the
same simple interface to the artists and visitors.

The sites can be integrated with [last.fm](http://last.fm) and
[Jamendo](http://jamendo.com), and (in later releases) other social-networking
sites.
